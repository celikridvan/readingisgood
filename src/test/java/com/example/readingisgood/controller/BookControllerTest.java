package com.example.readingisgood.controller;

import com.example.readingisgood.entity.Book;
import com.example.readingisgood.rest.BookController;
import com.example.readingisgood.service.BookService;
import com.example.readingisgood.util.ServiceTestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
public class BookControllerTest {

    @InjectMocks
    private BookController bookController;

    @Mock
    private BookService bookService;

    @Before
    public void init() {

        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void bookFindAllList() {
        List<Book> bookList = ServiceTestUtil.generateRandomBookList();
        when(bookService.findAll(0, 1)).thenReturn(bookList);
        Assertions.assertEquals(bookList.get(0).getAuthor(), bookController.readAll(0, 1).get(0).getAuthor());
        Assertions.assertEquals(bookList.get(0).getTitle(), bookController.readAll(0, 1).get(0).getTitle());
        Assertions.assertEquals(bookList.get(0).getStock(), bookController.readAll(0, 1).get(0).getStock());
        Assertions.assertEquals(bookList.get(0).getAmount(), bookController.readAll(0, 1).get(0).getAmount());
    }


    @Test
    public void bookFindAllListEmpty() {
        when(bookService.findAll(0, 1)).thenReturn(Collections.EMPTY_LIST);
        Assertions.assertEquals(Collections.EMPTY_LIST, bookService.findAll(0, 1));
    }

    @Test
    public void bookFindOne() {
        Book book = ServiceTestUtil.generateRandomBook();
        when(bookService.findOne(book.getId())).thenReturn(book);
        Assertions.assertEquals(book.getId(), bookController.read(book.getId()).getId());
    }

    @Test
    public void bookSave() {
        Book book = ServiceTestUtil.generateRandomBook();
        when(bookService.save(book)).thenReturn(book);
        Assertions.assertEquals(book.getId(), bookController.create(book).getId());
        Assertions.assertEquals(book.getTitle(), bookController.create(book).getTitle());
    }

    @Test
    public void bookUpdate() {
        Book book = ServiceTestUtil.generateRandomBook();
        when(bookService.update(book)).thenReturn(book);
        Assertions.assertEquals(book.getId(), bookController.update(book).getId());
        Assertions.assertEquals(book.getTitle(), bookController.update(book).getTitle());
    }

}
