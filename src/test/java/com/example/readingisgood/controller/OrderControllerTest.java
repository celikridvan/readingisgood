package com.example.readingisgood.controller;

import com.example.readingisgood.entity.Customer;
import com.example.readingisgood.entity.Order;
import com.example.readingisgood.model.CustomerOrderStatistic;
import com.example.readingisgood.rest.OrderController;
import com.example.readingisgood.service.OrderService;
import com.example.readingisgood.util.ServiceTestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

public class OrderControllerTest {

    @InjectMocks
    private OrderController orderController;

    @Mock
    private OrderService orderService;

    @Before
    public void init() {

        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void bookFindAllList() {
        List<Order> orderList = ServiceTestUtil.generateRandomOrderList();
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);
        Calendar endDate = Calendar.getInstance();
        when(orderService.findAllBetweenDate(0, 1, startDate.getTime(), endDate.getTime())).thenReturn(orderList);

        Assertions.assertEquals(orderList.get(0).getId(), orderController.readAll(0, 1, startDate.getTime(), endDate.getTime()).get(0).getId());

    }


    @Test
    public void bookFindAllListEmpty() {
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);
        Calendar endDate = Calendar.getInstance();
        when(orderService.findAllBetweenDate(0, 1, startDate.getTime(), endDate.getTime())).thenReturn(Collections.EMPTY_LIST);
        Assertions.assertEquals(Collections.EMPTY_LIST, orderController.readAll(0, 1, startDate.getTime(), endDate.getTime()));
    }

    @Test
    public void getCustomerOrders() {
        Customer customer = ServiceTestUtil.generateRandomCustomer();
        List<Order> orderList = ServiceTestUtil.generateRandomOrderList();
        when(orderService.getCustomerOrders(customer.getId(), 0, 1)).thenReturn(orderList);
        Assertions.assertEquals(orderList.get(0).getId(), orderController.getCustomerOrders(customer.getId(), 0, 1).get(0).getId());
    }

    @Test
    public void queryMonthlyStatistics() {
        Customer customer = ServiceTestUtil.generateRandomCustomer();
        CustomerOrderStatistic customerOrderStatistic = new CustomerOrderStatistic("10",2,10,300.0);

        when(orderService.queryMonthlyStatistics(customer.getId())).thenReturn(Arrays.asList(customerOrderStatistic));
        Assertions.assertEquals(customerOrderStatistic.getTotalBookCount(), orderController.queryMonthlyStatistics(customer.getId()).get(0).getTotalBookCount());
    }


}

