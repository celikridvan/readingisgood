package com.example.readingisgood.controller;

import com.example.readingisgood.entity.Customer;
import com.example.readingisgood.rest.CustomerController;
import com.example.readingisgood.service.CustomerService;
import com.example.readingisgood.util.ServiceTestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
public class CustomerControllerTest {

    @InjectMocks
    private CustomerController customerController;

    @Mock
    private CustomerService customerService;

    @Before
    public void init() {

        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void customerFindAllList() {
        List<Customer> customerList = ServiceTestUtil.generateRandomCustomerList();
        when(customerService.findAll(0, 1)).thenReturn(customerList);
        Assertions.assertEquals(customerList.get(0).getId(), customerController.readAll(0, 1).get(0).getId());
        Assertions.assertEquals(customerList.get(0).getSurname(), customerController.readAll(0, 1).get(0).getSurname());
        Assertions.assertEquals(customerList.get(0).getName(), customerController.readAll(0, 1).get(0).getName());
        Assertions.assertEquals(customerList.get(0).getEmail(), customerController.readAll(0, 1).get(0).getEmail());
    }


    @Test
    public void customerFindAllListEmpty() {
        when(customerService.findAll(0, 1)).thenReturn(Collections.EMPTY_LIST);
        Assertions.assertEquals(Collections.EMPTY_LIST, customerService.findAll(0, 1));
    }

    @Test
    public void customerFindOne() {
        Customer customer = ServiceTestUtil.generateRandomCustomer();
        when(customerService.findOne(customer.getId())).thenReturn(customer);
        Assertions.assertEquals(customer.getId(), customerController.read(customer.getId()).getId());
    }

    @Test
    public void customerUpdate() {
        Customer customer = ServiceTestUtil.generateRandomCustomer();
        when(customerService.update(customer)).thenReturn(customer);
        Assertions.assertEquals(customer.getId(), customerController.update(customer).getId());
        Assertions.assertEquals(customer.getName(), customerController.update(customer).getName());
    }

}
