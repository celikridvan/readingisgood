package com.example.readingisgood.util;

import com.example.readingisgood.entity.Book;
import com.example.readingisgood.entity.Customer;
import com.example.readingisgood.entity.Order;
import com.example.readingisgood.entity.Role;

import java.util.*;

public class ServiceTestUtil {

    public static Book generateRandomBook() {
        Book book = new Book();
        book.setId("testId");
        book.setTitle("Title");
        book.setAuthor("Author");
        book.setPublishedDate(new Date());
        book.setStock(10);
        book.setAmount(120.5);
        return book;
    }

    public static List<Book> generateRandomBookList(){
        return Arrays.asList(generateRandomBook());
    }

    public static Customer generateRandomCustomer() {
        Customer customer = new Customer();
        customer.setEmail("ridvan@mail.com");
        customer.setPassword("test123");
        customer.setName("ridvan");
        customer.setSurname("Surnam");
        customer.setId("TestId");
        Role role = new Role();
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        customer.setRoles(roles);
        return customer;
    }

    public static List<Customer> generateRandomCustomerList(){
        return Arrays.asList(generateRandomCustomer());
    }

    public static Order generateRandomOrder() {
        Order order = new Order(
                generateRandomBook(),
                generateRandomCustomer(),
                1
        );
        order.setId("TestId");
        order.setTotalOrderAmount(order.getBook().getAmount()*order.getOrderCount());
        return  order;
    }

    public static List<Order> generateRandomOrderList(){
        return Arrays.asList(generateRandomOrder());
    }
}
