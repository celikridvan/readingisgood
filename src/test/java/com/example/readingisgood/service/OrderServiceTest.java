package com.example.readingisgood.service;

import com.example.readingisgood.entity.Book;
import com.example.readingisgood.entity.Customer;
import com.example.readingisgood.entity.Order;
import com.example.readingisgood.exception.DatabaseUpdateFailureException;
import com.example.readingisgood.model.OrderRequest;
import com.example.readingisgood.repository.BookRepository;
import com.example.readingisgood.repository.CustomerRepository;
import com.example.readingisgood.repository.OrderRepository;
import com.example.readingisgood.util.ServiceTestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class OrderServiceTest {

    @InjectMocks
    private OrderService orderService;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private BookRepository bookRepository;

    @Mock
    private CustomerRepository customerRepository;

    @Before
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void orderFindAllList() {
        List<Order> orderList = ServiceTestUtil.generateRandomOrderList();
        Page<Order> pageMockOrderList = new PageImpl<>(orderList);
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);
        Calendar endDate = Calendar.getInstance();
        when(orderRepository.findAllByCreateDateBetween(startDate.getTime(),endDate.getTime(),PageRequest.of(0, 1))).thenReturn(pageMockOrderList);
        Assertions.assertEquals(orderList.get(0).getCustomer().getId(),
                orderService.findAllBetweenDate(0, 1,
                startDate.getTime(),
                endDate.getTime()
            ).get(0).getCustomer().getId());
    }

    @Test
    public void orderFindAllListEmpty() {
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);
        Calendar endDate = Calendar.getInstance();
        Page<Order> pageMockOrderList = new PageImpl<>(Collections.EMPTY_LIST);
        when(orderRepository.findAllByCreateDateBetween(startDate.getTime(),endDate.getTime(),PageRequest.of(0, 1))).thenReturn(pageMockOrderList);
        Assertions.assertEquals(Collections.EMPTY_LIST,
                orderService.findAllBetweenDate(0, 1,
                        startDate.getTime(),
                        endDate.getTime()
                ));
    }

    @Test
    public void orderFindOne() {
        Order order = ServiceTestUtil.generateRandomOrder();
        Optional<Order> optionalMockOrder = Optional.of(order);

        when(orderRepository.findById(order.getId())).thenReturn(optionalMockOrder);
        Assertions.assertEquals(order.getId(), orderService.findOne(order.getId()).getId());
    }

    @Test
    public void orderSave() {
        Order order = ServiceTestUtil.generateRandomOrder();
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setOrderCount(order.getOrderCount());
        orderRequest.setCustomerId(order.getCustomer().getId());
        orderRequest.setBookId(order.getBook().getId());

        Optional<Book> optionalMockBook = Optional.of(order.getBook());
        Optional<Customer> optionalMockCustomer = Optional.of(order.getCustomer());

        when(customerRepository.findById(order.getCustomer().getId())).thenReturn(optionalMockCustomer);
        when(bookRepository.findById(order.getBook().getId())).thenReturn(optionalMockBook);
        when(bookRepository.save(order.getBook())).thenReturn(order.getBook());
        when(orderRepository.save(any(Order.class))).thenReturn(order);

        Assertions.assertEquals(order.getId(), orderService.save(orderRequest).getId());
        Assertions.assertEquals(order.getTotalOrderAmount(), orderService.save(orderRequest).getTotalOrderAmount());
    }

    @Test
    public void shouldThrowException() {
        Order order = ServiceTestUtil.generateRandomOrder();
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setOrderCount(order.getOrderCount());
        orderRequest.setCustomerId(order.getCustomer().getId());
        orderRequest.setBookId(order.getBook().getId());

        Optional<Book> optionalMockBook = Optional.of(order.getBook());
        Optional<Customer> optionalMockCustomer = Optional.of(order.getCustomer());

        when(customerRepository.findById(order.getCustomer().getId())).thenReturn(optionalMockCustomer);
        when(bookRepository.findById(order.getBook().getId())).thenReturn(optionalMockBook);
        when(bookRepository.save(order.getBook())).thenReturn(order.getBook());
        when(orderRepository.save(any(Order.class))).thenReturn(order);

        orderRequest.setOrderCount(Integer.MAX_VALUE);
        assertThatThrownBy(() -> orderService.save(orderRequest))
                .isInstanceOf(DatabaseUpdateFailureException.class)
                .hasMessageStartingWith("Insufficient Book stock ID = " + orderRequest.getBookId());
    }

    @Test
    public void queryMonthlyStatistics() {
        Order order = ServiceTestUtil.generateRandomOrder();
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setOrderCount(order.getOrderCount());
        orderRequest.setCustomerId(order.getCustomer().getId());
        orderRequest.setBookId(order.getBook().getId());

        Optional<Book> optionalMockBook = Optional.of(order.getBook());
        Optional<Customer> optionalMockCustomer = Optional.of(order.getCustomer());

        when(customerRepository.findById(order.getCustomer().getId())).thenReturn(optionalMockCustomer);
        when(bookRepository.findById(order.getBook().getId())).thenReturn(optionalMockBook);
        when(bookRepository.save(order.getBook())).thenReturn(order.getBook());
        when(orderRepository.save(any(Order.class))).thenReturn(order);

        Assertions.assertEquals(order.getId(), orderService.save(orderRequest).getId());
        Assertions.assertEquals(order.getTotalOrderAmount(), orderService.save(orderRequest).getTotalOrderAmount());
    }
}
