package com.example.readingisgood.service;

import com.example.readingisgood.entity.Customer;
import com.example.readingisgood.entity.Role;
import com.example.readingisgood.exception.DatabaseUpdateFailureException;
import com.example.readingisgood.repository.CustomerRepository;
import com.example.readingisgood.repository.RoleRepository;
import com.example.readingisgood.util.ServiceTestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
public class CustomerServiceTest {

    @InjectMocks
    private CustomerService customerService;

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private PasswordEncoder bCryptPasswordEncoder;

    @Before
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void customerFindAllList() {
        List<Customer> customerList = ServiceTestUtil.generateRandomCustomerList();
        Page<Customer> pageMockCustomerList = new PageImpl<>(customerList);

        when(customerRepository.findAll(PageRequest.of(0, 1))).thenReturn(pageMockCustomerList);
        Assertions.assertEquals(customerList.get(0).getEmail(), customerService.findAll(0, 1).get(0).getEmail());
        Assertions.assertEquals(customerList.get(0).getName(), customerService.findAll(0, 1).get(0).getName());
        Assertions.assertEquals(customerList.get(0).getSurname(), customerService.findAll(0, 1).get(0).getSurname());
        Assertions.assertEquals(customerList.get(0).getId(), customerService.findAll(0, 1).get(0).getId());
    }

    @Test
    public void customerFindAllListEmpty() {
        Page<Customer> pageMockCustomerList = new PageImpl<>(Collections.EMPTY_LIST);

        when(customerRepository.findAll(PageRequest.of(0, 1))).thenReturn(pageMockCustomerList);
        Assertions.assertEquals(Collections.EMPTY_LIST, customerService.findAll(0, 1));
    }

    @Test
    public void customerFindOne() {
        Customer customer = ServiceTestUtil.generateRandomCustomer();
        Optional<Customer> optionalMockCustomer = Optional.of(customer);

        when(customerRepository.findById(customer.getId())).thenReturn(optionalMockCustomer);
        Assertions.assertEquals(customer.getId(), customerService.findOne(customer.getId()).getId());
    }

    @Test
    public void customerSave() {
        Customer customer = ServiceTestUtil.generateRandomCustomer();
        when(bCryptPasswordEncoder.encode(customer.getPassword())).thenReturn(customer.getPassword());
        when(customerRepository.save(customer)).thenReturn(customer);
        Assertions.assertEquals(customer.getId(), customerService.save(customer).getId());
        Assertions.assertEquals(customer.getEmail(), customerService.save(customer).getEmail());
    }

    @Test
    public void customerSaveDefaultRole() {
        Customer customer = ServiceTestUtil.generateRandomCustomer();
        Role role = new Role();
        role.setRole("USER");
        customer.setRoles(null);
        when(bCryptPasswordEncoder.encode(customer.getPassword())).thenReturn(customer.getPassword());
        when(customerRepository.save(customer)).thenReturn(customer);
        when(roleRepository.findByRole(any(String.class))).thenReturn(Optional.of(role));
        Assertions.assertEquals(customer.getId(), customerService.save(customer).getId());
        Assertions.assertEquals(customer.getEmail(), customerService.save(customer).getEmail());
    }

    @Test
    public void customerUpdate() {
        Customer customer = ServiceTestUtil.generateRandomCustomer();
        Optional<Customer> optionalMockCustomer = Optional.of(customer);
        when(customerRepository.findById(customer.getId())).thenReturn(optionalMockCustomer);
        when(customerRepository.save(customer)).thenReturn(customer);
        Assertions.assertEquals(customer.getId(), customerService.update(customer).getId());
        Assertions.assertEquals(customer.getEmail(), customerService.update(customer).getEmail());
    }

    @Test
    public void shouldThrowException() {
        Customer customer = ServiceTestUtil.generateRandomCustomer();
        Optional<Customer> optionalMockCustomer = Optional.of(customer);
        when(customerRepository.findById(customer.getId())).thenReturn(optionalMockCustomer);
        when(customerRepository.save(customer)).thenReturn(customer);
        customer.setId("idNotExist");
        assertThatThrownBy(() -> customerService.update(customer))
                .isInstanceOf(DatabaseUpdateFailureException.class)
                .hasMessageStartingWith("Customer not found given ID = " + customer.getId());
    }
}
