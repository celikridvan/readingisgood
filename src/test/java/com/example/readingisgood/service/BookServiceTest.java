package com.example.readingisgood.service;

import com.example.readingisgood.entity.Book;
import com.example.readingisgood.exception.NoDataFoundException;
import com.example.readingisgood.repository.BookRepository;
import com.example.readingisgood.util.ServiceTestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import static org.mockito.Mockito.when;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class BookServiceTest {

    @InjectMocks
    private BookService bookService;

    @Mock
    private BookRepository bookRepository;

    @Before
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void bookFindAllList() {
        List<Book> bookList = ServiceTestUtil.generateRandomBookList();
        Page<Book> pageMockBookList = new PageImpl<>(bookList);

        when(bookRepository.findAll(PageRequest.of(0, 1))).thenReturn(pageMockBookList);
        Assertions.assertEquals(bookList.get(0).getAuthor(), bookService.findAll(0, 1).get(0).getAuthor());
        Assertions.assertEquals(bookList.get(0).getTitle(), bookService.findAll(0, 1).get(0).getTitle());
        Assertions.assertEquals(bookList.get(0).getStock(), bookService.findAll(0, 1).get(0).getStock());
        Assertions.assertEquals(bookList.get(0).getAmount(), bookService.findAll(0, 1).get(0).getAmount());
    }

    @Test
    public void bookFindAllListEmpty() {
        Page<Book> pageMockBookList = new PageImpl<>(Collections.EMPTY_LIST);

        when(bookRepository.findAll(PageRequest.of(0, 1))).thenReturn(pageMockBookList);
        Assertions.assertEquals(Collections.EMPTY_LIST, bookService.findAll(0, 1));
    }

    @Test
    public void bookFindOne() {
        Book book = ServiceTestUtil.generateRandomBook();
        Optional<Book> optionalMockBook = Optional.of(book);

        when(bookRepository.findById(book.getId())).thenReturn(optionalMockBook);
        Assertions.assertEquals(book.getId(), bookService.findOne(book.getId()).getId());
    }

    @Test
    public void bookSave() {
        Book book = ServiceTestUtil.generateRandomBook();
        when(bookRepository.save(book)).thenReturn(book);
        Assertions.assertEquals(book.getId(), bookService.save(book).getId());
        Assertions.assertEquals(book.getTitle(), bookService.save(book).getTitle());
    }

    @Test
    public void bookUpdate() {
        Book book = ServiceTestUtil.generateRandomBook();
        Optional<Book> optionalMockBook = Optional.of(book);
        when(bookRepository.findById(book.getId())).thenReturn(optionalMockBook);
        when(bookRepository.save(book)).thenReturn(book);
        Assertions.assertEquals(book.getId(), bookService.update(book).getId());
        Assertions.assertEquals(book.getTitle(), bookService.update(book).getTitle());
    }

    @Test
    public void shouldThrowException() {
        Book book = ServiceTestUtil.generateRandomBook();
        Optional<Book> optionalMockBook = Optional.of(book);
        when(bookRepository.findById(book.getId())).thenReturn(optionalMockBook);
        when(bookRepository.save(book)).thenReturn(book);
        book.setId("idNotExist");
        assertThatThrownBy(() -> bookService.update(book))
                .isInstanceOf(NoDataFoundException.class)
                .hasMessageStartingWith("Book not found given ID = " + book.getId());
    }
}
