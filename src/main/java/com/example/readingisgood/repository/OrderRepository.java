package com.example.readingisgood.repository;

import com.example.readingisgood.entity.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;

public interface OrderRepository extends MongoRepository<Order, String> {

    List<Order> findByCustomerId(String customerId);

    Page<Order> findByCustomerId(String customerId, Pageable pageable);

    List<Order> findAllByCreateDateBetween(Date startDate, Date endDate);

    Page<Order> findAllByCreateDateBetween(Date startDate, Date endDate, Pageable pageable);
}
