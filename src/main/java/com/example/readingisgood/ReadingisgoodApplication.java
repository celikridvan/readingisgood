package com.example.readingisgood;

import com.example.readingisgood.entity.Role;
import com.example.readingisgood.repository.RoleRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Optional;

@SpringBootApplication
public class ReadingisgoodApplication {

    public static void main(String[] args) {

        SpringApplication.run(ReadingisgoodApplication.class, args);
    }

    @Bean
    CommandLineRunner init(RoleRepository roleRepository) {

        return args -> {

            Optional<Role> roleOptional = roleRepository.findByRole("USER");
            if (!roleOptional.isPresent()) {
                Role newUserRole = new Role();
                newUserRole.setRole("USER");
                roleRepository.save(newUserRole);
            }
        };

    }
}
