package com.example.readingisgood.entity;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Document(collection = "order")
public class Order {

    @Id
    private String id;

    @CreatedDate
    private Date createDate;

    @NotNull
    @Min(value = 1, message = "Invalid Order Count")
    private Integer orderCount;

    @NotNull
    @Min(value = 0, message = "Invalid Total Order Amount")
    private Double totalOrderAmount;

    @NotNull
    @DBRef
    private Book book;

    @NotNull
    @DBRef
    private Customer customer;

    public Order(@NotNull Book book, @NotNull Customer customer, @NotNull @Min(value = 1, message = "Invalid Order Count") Integer orderCount) {
        this.orderCount = orderCount;
        this.book = book;
        this.customer = customer;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public Date getCreateDate() {

        return createDate;
    }

    public void setCreateDate(Date createDate) {

        this.createDate = createDate;
    }

    public Integer getOrderCount() {

        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {

        this.orderCount = orderCount;
    }

    public Double getTotalOrderAmount() {
        return totalOrderAmount;
    }

    public void setTotalOrderAmount(Double totalOrderAmount) {
        this.totalOrderAmount = totalOrderAmount;
    }

    public Book getBook() {

        return book;
    }

    public void setBook(Book book) {

        this.book = book;
    }

    public Customer getCustomer() {

        return customer;
    }

    public void setCustomer(Customer customer) {

        this.customer = customer;
    }
}
