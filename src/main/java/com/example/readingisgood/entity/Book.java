package com.example.readingisgood.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Document(collection = "book")
public class Book {

    @Id
    private String id;

    @NotNull
    private String title;

    @NotNull
    private String author;

    @NotNull
    @JsonFormat(pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date publishedDate;

    @NotNull
    @Min(value = 0, message = "A negative stock is not allowed")
    private Integer stock;

    @NotNull
    @Min(value = 0, message = "Amount must be positive")
    private Double amount;

    @JsonIgnore
    @CreatedDate
    private Date createDate;

    public String getId() {

        return id;
    }

    public void setId(String id) {

        this.id = id;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getAuthor() {

        return author;
    }

    public void setAuthor(String author) {

        this.author = author;
    }

    public Date getPublishedDate() {

        return publishedDate;
    }

    public void setPublishedDate(Date publishedDate) {

        this.publishedDate = publishedDate;
    }

    public Date getCreateDate() {

        return createDate;
    }

    public void setCreateDate(Date createDate) {

        this.createDate = createDate;
    }

    public Integer getStock() {

        return stock;
    }

    public void setStock(Integer stock) {

        this.stock = stock;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

}
