package com.example.readingisgood.rest;

import com.example.readingisgood.entity.Customer;
import com.example.readingisgood.entity.Order;
import com.example.readingisgood.model.CustomerOrderStatistic;
import com.example.readingisgood.model.OrderRequest;
import com.example.readingisgood.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Order read(@PathVariable("id") String id) {
        return orderService.findOne(id);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Order> readAll(
            @RequestParam(value = "page", defaultValue = "0", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "startDate", required = true) @DateTimeFormat(pattern = "dd-MM-yyyy") Date startDate,
            @RequestParam(value = "endDate", required = true) @DateTimeFormat(pattern = "dd-MM-yyyy") Date endDate) {

        return orderService.findAllBetweenDate(page, size, startDate, endDate);

    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Order create(@Valid @RequestBody OrderRequest orderRequest) {

        return orderService.save(orderRequest);
    }

    @RequestMapping(value = "/queryMonthlyStatistics/{customerId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CustomerOrderStatistic> queryMonthlyStatistics(@PathVariable("customerId") String customerId) {
        return orderService.queryMonthlyStatistics(customerId);
    }


    @RequestMapping(value = "/customerOrders", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Order> getCustomerOrders(
            @RequestParam(value = "customerId", required = true) String customerId,
            @RequestParam(value = "page", defaultValue = "0", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size) {

        return orderService.getCustomerOrders(customerId, page, size);
    }

}
