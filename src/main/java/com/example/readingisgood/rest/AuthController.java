package com.example.readingisgood.rest;

import com.example.readingisgood.entity.Customer;
import com.example.readingisgood.model.AuthRequest;
import com.example.readingisgood.model.AuthResponse;
import com.example.readingisgood.security.JwtTokenProvider;
import com.example.readingisgood.service.CustomUserDetailsService;
import com.example.readingisgood.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public AuthResponse login(@RequestBody AuthRequest authRequest) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getEmail(), authRequest.getPassword()));
            AuthResponse authResponse = new AuthResponse();
            authResponse.setEmail(authRequest.getEmail());

            authResponse.setToken(jwtTokenProvider.createToken(authRequest.getEmail(),
                    this.customUserDetailsService.findUserByEmail(authRequest.getEmail()).getRoles()));

            return authResponse;
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid email/password");
        }
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Customer create(@Valid @RequestBody Customer customer) {

        return customerService.save(customer);
    }
}
