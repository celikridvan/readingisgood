package com.example.readingisgood.service;

import com.example.readingisgood.entity.Role;
import com.example.readingisgood.exception.DatabaseUpdateFailureException;
import com.example.readingisgood.exception.NoDataFoundException;
import com.example.readingisgood.entity.Customer;
import com.example.readingisgood.repository.CustomerRepository;
import com.example.readingisgood.repository.RoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    private static final Logger logger = LoggerFactory.getLogger(CustomerService.class);

    public List<Customer> findAll(Integer page, Integer size) {
        logger.info("findAll page:" + page + " size" + size);
        if (size == null)
            return customerRepository.findAll();
        else {
            Pageable firstPageWithTwoElements = PageRequest.of(page, size);
            return customerRepository.findAll(firstPageWithTwoElements).getContent();
        }
    }

    public Customer findOne(String id) {
        logger.info("findOne Id:" + id);
        Optional<Customer> customerOptional = customerRepository.findById(id);
        if (customerOptional.isPresent()) {
            logger.info("findOne Id:" + id + " Return:" + customerOptional.get());
            return customerOptional.get();
        } else {
            logger.info("findOne Id:" + id + " Customer not found given ID = " + id);
            throw new NoDataFoundException("Customer not found given ID = " + id);
        }
    }

    public Customer update(Customer customer) {
        logger.info("Customer Update: " + customer.toString());
        if (customer.getId() == null) {
            logger.info("Customer Update: Customer id is missing");
            throw new DatabaseUpdateFailureException("Customer id is missing");
        }
        Optional<Customer> customerOptional = customerRepository.findById(customer.getId());
        if (customerOptional.isPresent()) {
            return customerRepository.save(customer);
        } else {
            logger.info("Customer Update: " + "Customer not found given ID = " + customer.getId());
            throw new DatabaseUpdateFailureException("Customer not found given ID = " + customer.getId());
        }
    }

    public Customer save(Customer customer) {
        logger.info("Customer Save: " + customer.toString());
        if(customer.getRoles() == null || customer.getRoles().size() == 0){
            Role role = roleRepository.findByRole("USER").get();
            customer.setRoles(new HashSet<>(Arrays.asList(role)));
        }
        customer.setPassword(bCryptPasswordEncoder.encode(customer.getPassword()));
        Optional<Customer> customerOptional = customerRepository.findByEmail(customer.getEmail());
        if (customerOptional.isPresent()) {
            logger.info("Customer Save: " + "Email address already exists = " + customer.getEmail());
            throw new DatabaseUpdateFailureException("Email address already exists = " + customer.getEmail());
        }
        return customerRepository.save(customer);

    }

}
