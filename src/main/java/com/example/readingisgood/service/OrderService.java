package com.example.readingisgood.service;

import com.example.readingisgood.entity.Customer;
import com.example.readingisgood.exception.DatabaseUpdateFailureException;
import com.example.readingisgood.exception.NoDataFoundException;
import com.example.readingisgood.entity.Book;
import com.example.readingisgood.entity.Order;
import com.example.readingisgood.model.CustomerOrderStatistic;
import com.example.readingisgood.model.OrderRequest;
import com.example.readingisgood.repository.CustomerRepository;
import com.example.readingisgood.repository.OrderRepository;
import com.example.readingisgood.repository.BookRepository;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    private static final Logger logger = LoggerFactory.getLogger(OrderService.class);

    public List<Order> findAllBetweenDate(Integer page, Integer size, Date starDate, Date endDate) {
        logger.info("findAll page:" + page + " size " + size + " starDate " + starDate +" endDate " +endDate);
        if (size == null)
            return orderRepository.findAllByCreateDateBetween(starDate, endDate);
        else {
            Pageable firstPageWithTwoElements = PageRequest.of(page, size);
            return orderRepository.findAllByCreateDateBetween(starDate, endDate, firstPageWithTwoElements).getContent();
        }
    }

    public Order findOne(String id) {
        logger.info("findOne Id:" + id);
        Optional<Order> optionalOrder = orderRepository.findById(id);
        if (optionalOrder.isPresent()) {
            logger.info("findOne Id:" + id + " Return:" + optionalOrder.get());
            return optionalOrder.get();
        } else {
            logger.info("findOne Id:" + id + " Order not found given ID = " + id);
            throw new NoDataFoundException("Order not found given ID = " + id);
        }
    }

    public Order save(OrderRequest orderRequest) {
        logger.info("save :" + orderRequest);
        Optional<Book> bookOptional = bookRepository.findById(orderRequest.getBookId());
        if (!bookOptional.isPresent()) {
            logger.info("save " + orderRequest + " Book not found given ID = " + orderRequest.getBookId());
            throw new NoDataFoundException("Book not found given ID = " + orderRequest.getBookId());
        }
        Book book = bookOptional.get();
        if (book.getStock() - orderRequest.getOrderCount() < 0) {
            logger.info("save Id:" + orderRequest + " Insufficient Book stock ID = " + book.getId());
            throw new DatabaseUpdateFailureException("Insufficient Book stock ID = " + book.getId());
        }

        Optional<Customer> customerOptional = customerRepository.findById(orderRequest.getCustomerId());
        if (!customerOptional.isPresent()) {
            logger.info("save " + orderRequest + "Customer not found given ID = " + orderRequest.getCustomerId());
            throw new NoDataFoundException("Customer not found given ID = " + orderRequest.getCustomerId());
        }
        Customer customer = customerOptional.get();

        Order order = new Order(book, customer, orderRequest.getOrderCount());
        book.setStock(book.getStock() - orderRequest.getOrderCount());
        order.setTotalOrderAmount(order.getOrderCount() * book.getAmount());
        bookRepository.save(book);
        return orderRepository.save(order);
    }

    public List<Order> getCustomerOrders(String customerId, Integer page, Integer size) {
        logger.info("getCustomerOrders page:" + page + " size" + size);
        if (size == null)
            return orderRepository.findByCustomerId(customerId);
        else {
            Pageable pageable = PageRequest.of(page, size);
            return orderRepository.findByCustomerId(customerId, pageable).getContent();
        }
    }

    public List<CustomerOrderStatistic> queryMonthlyStatistics(String customerId) {
        logger.info("queryMonthlyStatistics customer:" + customerId);

        Optional<Customer> customerOptional = customerRepository.findById(customerId);
        if (!customerOptional.isPresent()) {
            logger.info("queryMonthlyStatistics" + " Customer not found given ID = " + customerId);
            throw new NoDataFoundException("Customer not found given ID = " + customerId);
        }
        Customer customer = customerOptional.get();

        Aggregation agg = Aggregation.newAggregation(
                Aggregation.unwind("customer"),
                Aggregation.project("date", "orderCount", "totalOrderAmount", "customer")
                        .and(DateOperators.Month.monthOf("createDate")).as("month"),
                Aggregation.match(Criteria.where("customer.$id").is(new ObjectId(customer.getId()))),
                Aggregation.group("month")
                        .count().as("totalOrderCount")
                        .first("month").as("month")
                        .sum("orderCount").as("totalBookCount")
                        .sum("totalOrderAmount").as("totalPurchasedAmount"));

        List<CustomerOrderStatistic> result = mongoTemplate.aggregate(agg,
                mongoTemplate.getCollectionName(Order.class),
                CustomerOrderStatistic.class)
                .getMappedResults();
        logger.info("queryMonthlyStatistics Return: " + result);

        return result;
    }
}
