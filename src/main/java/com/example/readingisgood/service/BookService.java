package com.example.readingisgood.service;

import com.example.readingisgood.exception.NoDataFoundException;
import com.example.readingisgood.entity.Book;
import com.example.readingisgood.repository.BookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class BookService {

    @Autowired
    BookRepository bookRepository;

    private static final Logger logger = LoggerFactory.getLogger(BookService.class);

    public List<Book> findAll(Integer page, Integer size) {
        logger.info("findAll page:" + page + " size" + size);
        if (size == null)
            return bookRepository.findAll();
        else {
            return bookRepository.findAll(PageRequest.of(page, size)).getContent();
        }
    }

    public Book findOne(String id) {
        logger.info("findOne Id:" + id);
        Optional<Book> bookOptional = bookRepository.findById(id);
        if (bookOptional.isPresent()) {
            logger.info("findOne Id:" + id + " Return:" + bookOptional.get());
            return bookOptional.get();
        } else {
            logger.info("findOne Id:" + id + " Book not found given ID = " + id);
            throw new NoDataFoundException("Book not found given ID = " + id);
        }
    }

    public Book update(Book book) {
        logger.info("Book Update: " + book.toString());
        if (book.getId() == null) {
            logger.info("Book Update: Book id is missing");
            throw new NoDataFoundException("Book id is missing");
        }
        Optional<Book> bookOptional = bookRepository.findById(book.getId());
        if (bookOptional.isPresent()) {
            return bookRepository.save(book);
        } else {
            logger.info("Book Update: " + "Book not found given ID = " + book.getId());
            throw new NoDataFoundException("Book not found given ID = " + book.getId());
        }
    }

    public Book save(Book book) {
        logger.info("Book Save: " + book.toString());
        return bookRepository.save(book);

    }
}
